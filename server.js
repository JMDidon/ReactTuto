'use strict';

const koa = require('koa');
const serve = require('koa-static');
const router = require('koa-router')();
const PORT = 8800;

let io = require('socket.io');
let app = koa();
let commentList = [
    { id: 0, author: 'Someone', text: 'A *nice* comment' },
    { id: 1, author: 'Another someone', text: '`Another` *nice* comment' }
];

router.get('/api/comments', function *(next) {
    this.body = commentList;
});

app.use(function *(next) {
    this.set('Access-Control-Allow-Origin',  'http://localhost:8888');
    this.set('Access-Control-Allow-Credentials',  true);
    yield next;
});

app.use(serve('www'));
app.use(router.routes());

io = io.listen(app.listen(PORT));
io.on('connection', function (socket) {
    socket.on('comment:new', function (msg) {
        msg.id = commentList.length;
        commentList.push(msg);

        socket.emit('comment:list-updated', commentList);
    });
});

console.log('Server started on port', PORT);
