let CommentForm = React.createClass({
    getInitialState: function () {
        return { author: '', text: '' };
    },
    handleAuthorChange: function (e) {
        this.setState({ author: e.target.value });
    },
    handleTextChange: function (e) {
        this.setState({ text: e.target.value });
    },
    handleSubmit: function (e) {
        let author = this.state.author.trim();
        let text = this.state.text.trim();

        if (!text || !author) {
            return;
        }

        this.props.onCommentSubmit({ author: author, text: text });
        this.setState({ author: '', text: '' });

        e.preventDefault();
    },
    render: function () {
        return (
            <form className="CommentForm" onSubmit={ this.handleSubmit }>
                <input
                    type="text"
                    placeholder="Username"
                    value={ this.state.author }
                    onChange={ this.handleAuthorChange } />

                <input
                    type="text"
                    placeholder="Comment"
                    value={ this.state.text }
                    onChange={ this.handleTextChange }/>

                <input type="submit" value="Submit" />
            </form>
        );
    }
});
