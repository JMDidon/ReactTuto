let CommentList = React.createClass({
    render: function () {
        let commentNodes = this.props.comments.map(comment => {
            return (
                <Comment author={ comment.author } key={ comment.id }>
                    { comment.text }
                </Comment>
            );
        });

        return (
            <div className="CommentList">
                { commentNodes }
            </div>
        );
    }
});
