let Comment = React.createClass({
    rawMarkup: function () {
        let md = new Remarkable();
        let rawMarkup = md.render(this.props.children.toString());

        return { __html: rawMarkup };
    },
    render: function () {
        return (
            <div className="Comment">
                <h3 className="Comment__author">{ this.props.author }</h3>
                <span dangerouslySetInnerHTML={ this.rawMarkup() }></span>
            </div>
        );
    }
});
