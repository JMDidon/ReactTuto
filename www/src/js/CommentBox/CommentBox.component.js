let CommentBox = React.createClass({
    getInitialState: function () {
        return { comments: [] };
    },
    getComments: function () {
        fetch(this.props.url)
            .then(parseJSON)
            .then(comments => {
                this.setState({ comments: comments });
            });
    },
    handleCommentSubmit: function (comment) {
        let socket = io('http://localhost:8800');
        socket.emit('comment:new', comment);

        socket.on('comment:list-updated', (commentList) => {
            this.setState({ comments: commentList });
        });
    },
    componentDidMount: function () {
        this.getComments();
    },
    render: function () {
        return (
            <div className="CommentBox">
                <h2 className="CommentBox__title">Comments</h2>
                <CommentList comments={ this.state.comments } />
                <CommentForm onCommentSubmit={ this.handleCommentSubmit }/>
            </div>
        );
    }
});

function parseJSON(rawComments) {
    return rawComments.json();
}
