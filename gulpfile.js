'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const plumber = require('gulp-plumber');
const nodemon = require('gulp-nodemon');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync');
const BROWSER_SYNC_RELOAD_DELAY = 500;

let resources = {
    scss: 'www/src/scss/**/*.*',
    js: ['www/src/js/CommentBox/*.*', 'www/src/js/*.*'],
    views: ['www/index.html', 'www/views/*.html', 'www/views/**/*.html'],
};

let options = {
    sass: {
        errLogToConsole: true
    }
};

gulp.task('nodemon', function (cb) {
    let called = false;

    return nodemon({
        script: 'server.js',
        watch: ['server.js']
    })
    .on('start', function onStart() {
        if (!called) {
            cb();
        }
        called = true;
    })
    .on('restart', function onRestart() {
        setTimeout(function reload() {
            browserSync.reload({
                stream: false
            });
        }, BROWSER_SYNC_RELOAD_DELAY);
    });
});

gulp.task('server', ['nodemon'], function () {
    browserSync.init({
        files: ['www/index.html'],
        proxy: 'http://localhost:8800',
        port: 8888,
        browser: ['google-chrome']
    });

    gulp.watch(resources.scss, ['build:css']);
    gulp.watch(resources.js, ['build:js']);
});

gulp.task('build:css', () => {
    return gulp.src(resources.scss)
        .pipe(sourcemaps.init())
            .pipe(plumber())
            .pipe(sass(options.sass).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('www/public/css'))
        .pipe(browserSync.stream());
});

gulp.task('build:js', () => {
    return gulp.src(resources.js)
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('www/public/js'))
        .pipe(browserSync.stream());
});

gulp.task('build:all', ['build:css', 'build:js']);
